/*------------------------------------------------------------------
 main.cpp
 Postfix Calculator
 
 This is the main class for the implementation of the Postfix Calculator.
 
 Created by Neemias Freitas
 ------------------------------------------------------------------*/

#include <iostream>
#include "Linked Queue.h"
#include "Postfix Calculator.h"

using namespace std;

int main(int argc, const char * argv[]) {
	// Declare variables
	string infixExpression, rpnExpression;
	double result;
	
	// Instantiation
	LQueue queue;
	
	// Get user's string
	cout << "Welcome to Postifix Calculator. This program converts your expression in infix notation to postfix notation and then evaluates it.\n-Legal operators are: +, -, *, /, % (mod), ^ (exponentiation), sqrt (square root) and ! (factorial).\n-You can use any real number. \n-Include spaces between all characters (even parentheses). \n-When using negative numbers, do not put space between the number and the minus.\n\nType your expression: ";
	getline(cin, infixExpression);
	
	// Convert from infix notation to postfix notation
	convert(infixExpression, queue);
	
	// Get string in postfix notation
	rpnExpression = queue.exportToString();
	
	// Evaluate postfix expression
	result = evaluate(queue);
	
	// Print out expression in postfix notation and its result
	cout << "\nYour epression in postfix notation is: " << rpnExpression << endl;
	cout << "The result of your expression is: " << result << endl;
}
