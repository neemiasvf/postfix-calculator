/*------------------------------------------------------------------
 Linked Queue.h
 Postfix Calculator
 
 A standard linked-list implementation of a queue class. Given the
 linked-list implementation, there is no upper limit on the number
 of elements that the queue can contain.
 
 Created by Neemias Freitas
 ------------------------------------------------------------------*/

#ifndef __Linked_Queue__Linked_Queue__
#define __Linked_Queue__Linked_Queue__

#include <iostream>
#include <string>
using namespace std;

// Definition of a queue node.
struct queueNode {
	string token;
	queueNode *next;
};

class LQueue{
	
public:
	// Constructors & destructors
	/*--------------------------------------------------------------
	 The default queue constructor provides the caller with an
	 initially empty queue.
	 --------------------------------------------------------------*/
	LQueue(void);
	
	/*--------------------------------------------------------------
	 The default queue destructor: deallocates all dynamically
	 allocated memory (i.e. queue nodes).
	 --------------------------------------------------------------*/
	~LQueue(void);
	
	
	// Modification Methods
	/*--------------------------------------------------------------
	 The enqueue method adds the provided string (token) to the queue.
	 
	 Precondition: none.
	 Postcondition: The number of elements being stored on the queue
	 is one greater than before.
	 --------------------------------------------------------------*/
	void enqueue(string token);
	
	/*--------------------------------------------------------------
	 The dequeue method removes the least recently added string (i.e. the head string), and returns its content to
	 the caller, via the reference parameter.
	 If the queue is empty, the return value is false, o.w. true.
	 
	 Precondition: none.
	 Postcondition: The number of elements being stored in the queue
	 is one less than before.
	 --------------------------------------------------------------*/
	bool dequeue(string &token);
	
	
	// Constant Methods
	/*--------------------------------------------------------------
	 The first method does not alter the contents of the queue.  It
	 returns to the caller the string that will be returned by the next dequeue.
	 
	 Preconditions: none.
	 Postconditions: none.
	 --------------------------------------------------------------*/
	string first(void) const;
	
	/*--------------------------------------------------------------
	 size is a constant member function that returns
	 the number of strings currently stored in the queue.
	 
	 Preconditions: none.
	 Postconditions: none.
	 --------------------------------------------------------------*/
	int size(void) const;
	
	/*------------------------------------------------------------------
	 print displays the contents of the queue.
	 
	 Preconditions: none.
	 Postconditions: none.
	 ------------------------------------------------------------------*/
	void print(void) const;
	
	/*------------------------------------------------------------------
	 exportToString returns a single string which is composed by the
	 concatenation of all strings stored in the queue.
	 
	 Preconditions: none.
	 Postconditions: none.
	 ------------------------------------------------------------------*/
	string exportToString(void) const;
	
	
private:
	// Object instance data
	queueNode *tail;
	int queueSize;
};

#endif /* defined(__Linked_Queue__Linked_Queue__) */