/*------------------------------------------------------------------
 Linked Stack.cpp
 Postfix Calculator
 
 A standard stack implementation using a linked-list implementation.
 
 Created by Neemias Freitas
 ------------------------------------------------------------------*/

#include "Linked Stack.h"

/*------------------------------------------------------------------
 The default LStack constructor.
 
 Preconditions: none.
 Postconditions: stackSize = 0 and top = NULL.
 ------------------------------------------------------------------*/
LStack::LStack(void) {
	top = NULL;
	stackSize = 0;
}

/*------------------------------------------------------------------
 The default LStack destructor.
 
 Preconditions: none.
 Postconditions: none
 ------------------------------------------------------------------*/
LStack::~LStack(void) {
	string token;
	while (pop(token));
}

/*------------------------------------------------------------------
 push inserts a new string into the stack.
 
 Preconditions: none.
 Postconditions: stackSize ++, top = temp.
 ------------------------------------------------------------------*/
void LStack::push(string token) {
	stackNode *temp = new stackNode;
	temp->token = token;
	temp->next = top;
	top = temp;
	stackSize++;
}

/*------------------------------------------------------------------
 pop returns to the user the string of top->value (if defined).
 
 Preconditions: none.
 Postconditions: stackSize--, top=top->next if top != NULL,
 then return true; o.w. return false.
 ------------------------------------------------------------------*/
bool LStack::pop(string &token) {
	if (top != NULL) {
		token = top->token;
		stackNode *temp = top;
		top = top->next;
		delete temp;
		stackSize--;
		return true;
	}
	return false;
}

/*------------------------------------------------------------------
 first returns to the user the string of top->token (if top != NULL). Otherwise, returns the string "NULL".
 
 Preconditions: none.
 Postconditions: none.
 ------------------------------------------------------------------*/
string LStack::first(void) const {
	if (top != NULL)
		return top->token;
	return "NULL";
}

/*------------------------------------------------------------------
 size returns the number of strings currently on the stack.
 
 Preconditions: none.
 Postconditions: none.
 ------------------------------------------------------------------*/
int LStack::size(void) const {
	return stackSize;
}

/*------------------------------------------------------------------
 print displays the content of the stack.
 
 Preconditions: none.
 Postconditions: none.
 ------------------------------------------------------------------*/
void LStack::print(void) const {
	if (top != NULL) {
		cout << "\nSize of the stack: " << stackSize << endl;
		int pos = 0;
		stackNode *current = top;
		while (current != NULL) {
			cout << "Position " << pos << " contains: " << current->token << endl;
			current = current->next;
			pos++;
		}
	}
	else
		cout << "\nStack is empty!\n";
}