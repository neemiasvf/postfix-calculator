/*------------------------------------------------------------------
 Postfix Calculator.h
 Postfix Calculator
 
 This program implements the RPN or Postifix calculator, including useful
 functions to convert infix to postfix notation, check operators,
 precedence and associativity, and to evaluate an expression.
 
 Created by Neemias Freitas
 ------------------------------------------------------------------*/
#ifndef __Postfix_Calculator__Postfix_Calculator__
#define __Postfix_Calculator__Postfix_Calculator__

#include <iostream>
#include <sstream>
#include <math.h>
#include "Linked Stack.h"
#include "Linked Queue.h"
using namespace std;

/*------------------------------------------------------------------
 convert converts any string provided as value parameter to postifx
 notation and allocates it into a linked queue given as a reference
 parameter.
 
 Preconditions: none.
 Postconditions: none.
 ------------------------------------------------------------------*/
void convert(string expression, LQueue &postfixExpression);

/*------------------------------------------------------------------
 isOperator verifies if the given string (token) is any valid operator.
 Valid operators are: +, -, *, /, % and ^.
 
 Preconditions: none.
 Postconditions: return true if token = any valid operator, o.w. return false.
 ------------------------------------------------------------------*/
bool isOperator(string token);

/*------------------------------------------------------------------
 checkPrecedence returns the precedence of the given operator.
 
 Preconditions: none.
 Postconditions: none.
 ------------------------------------------------------------------*/
int checkPrecedence(string op);

/*------------------------------------------------------------------
 checkAssociativity returns the associativity of the given operator.
 
 Preconditions: none.
 Postconditions: none.
 ------------------------------------------------------------------*/
string checkAssociativity(string op);

/*------------------------------------------------------------------
 evaluate evalutes the postfix expression conventionally allocated in a
 queue given as reference parameter.
 A double value will be returned as the result of the evaluation.
 
 Preconditions: none.
 Postconditions: queue will have been destructed at the end of the
 evaluation.
 ------------------------------------------------------------------*/
double evaluate(LQueue &postfixExpression);

/*------------------------------------------------------------------
 factorial returns the factorial of a given integer number n.
 
 Preconditions: none.
 Postconditions: none.
 ------------------------------------------------------------------*/
int factorial(int n);


#endif /* defined(__Postfix_Calculator__Postfix_Calculator__) */