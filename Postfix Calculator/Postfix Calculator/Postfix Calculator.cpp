/*------------------------------------------------------------------
 Postfix Calculator.cpp
 Postfix Calculator
 
 This program implements the RPN or Postifix calculator, including useful
 functions to convert infix to postfix notation, check operators,
 precedence and associativity, and to evaluate an expression.
 
 Created by Neemias Freitas
 ------------------------------------------------------------------*/

#include "Postfix Calculator.h"

/*------------------------------------------------------------------
 convert converts any string provided as value parameter to postifx
 notation and allocates it into a linked queue given as a reference
 parameter.
 
 Preconditions: none.
 Postconditions: none.
 ------------------------------------------------------------------*/
void convert(string expression, LQueue &postfixExpression) {
	istringstream iss(expression);
	string token, op;
	LStack auxStack;
	
	while (iss >> token) {
		if (isOperator(token)) {
			while ((checkAssociativity(token) == "left" && checkPrecedence(token) <= checkPrecedence(auxStack.first())) || (checkPrecedence(token) <= checkPrecedence(auxStack.first()))) {
				auxStack.pop(op);
				postfixExpression.enqueue(op);
			}
			auxStack.push(token);
		}
		
		else if (token == "(")
			auxStack.push(token);
		
		else if (token == ")") {
			while (auxStack.first() != "(") {
				if (auxStack.pop(op))
					postfixExpression.enqueue(op);
				else {
					cout << "There are mismatched parentheses! Please, check your expression!";
					exit(EXIT_FAILURE);
				}
			}
			auxStack.pop(op);
		}
		
		else
			postfixExpression.enqueue(token);
	}
	while (auxStack.pop(op)) {
		if (op == "(" || op == ")") {
			cout << "There are mismatched parentheses! Please, check your expression!";
			exit(EXIT_FAILURE);
		}
		else
			postfixExpression.enqueue(op);
	}
}

/*------------------------------------------------------------------
 isOperator verifies if the given string (token) is any valid operator.
 Valid operators are: +, -, *, /, % and ^.
 
 Preconditions: none.
 Postconditions: return true if token = any valid operator, o.w. return false.
 ------------------------------------------------------------------*/
bool isOperator(string token) {
	if (token == "+" || token == "-" || token == "*" || token == "/" || token == "%" || token == "^" || token == "sqrt" || token == "!")
		return true;
	return false;
}

/*------------------------------------------------------------------
 checkPrecedence returns the precedence of the given operator.
 
 Preconditions: none.
 Postconditions: none.
 ------------------------------------------------------------------*/
int checkPrecedence(string op) {
	if (op == "+" || op == "-")
		return 1;
	else if (op == "*" || op == "/" || op == "%")
		return 2;
	else if (op == "^" || op == "sqrt")
		return 3;
	else if (op == "!")
		return 4;
	return 0;
}

/*------------------------------------------------------------------
 checkAssociativity returns the associativity of the given operator.
 
 Preconditions: none.
 Postconditions: none.
 ------------------------------------------------------------------*/
string checkAssociativity(string op) {
	if (op == "^")
		return "right";
	return "left";
}

/*------------------------------------------------------------------
 evaluate evalutes the postfix expression conventionally allocated in a
 queue given as reference parameter.
 A double value will be returned as the result of the evaluation.
 
 Preconditions: none.
 Postconditions: queue will have been destructed at the end of the
 evaluation.
 ------------------------------------------------------------------*/
double evaluate(LQueue &postfixExpression) {
	string token;
	LStack auxStack;
	while (postfixExpression.dequeue(token)) {
		if (isOperator(token)) {
			if (auxStack.size() >= 2) {
				if (token == "+") {
					string value1, value2;
					auxStack.pop(value1);
					auxStack.pop(value2);
					auxStack.push(to_string(stod(value2) + stod(value1)));
				}
				else if (token == "-") {
					string value1, value2;
					auxStack.pop(value1);
					auxStack.pop(value2);
					auxStack.push(to_string(stod(value2) - stod(value1)));
				}
				else if (token == "*") {
					string value1, value2;
					auxStack.pop(value1);
					auxStack.pop(value2);
					auxStack.push(to_string(stod(value2) * stod(value1)));
				}
				else if (token == "/") {
					string value1, value2;
					auxStack.pop(value1);
					auxStack.pop(value2);
					auxStack.push(to_string(stod(value2) / stod(value1)));
				}
				else if (token == "%") {
					string value1, value2;
					auxStack.pop(value1);
					auxStack.pop(value2);
					auxStack.push(to_string(stoi(value2) % stoi(value1)));
				}
				else if (token == "^") {
					string value1, value2;
					auxStack.pop(value1);
					auxStack.pop(value2);
					auxStack.push(to_string(pow(stod(value2), stod(value1))));
				}
				else if (token == "sqrt") {
					string value;
					auxStack.pop(value);
					auxStack.push(to_string(sqrt(stod(value))));
				}
				else if (token == "!") {
					string value;
					auxStack.pop(value);
					auxStack.push(to_string(factorial(stoi(value))));
				}
				else {
					cout << "(Error) Please, enter a valid operator!\n'" << token << "' is not a valid one." << endl;
					exit(EXIT_FAILURE);
				}
			}
			else if (auxStack.size() == 1) {
				if (token == "sqrt") {
					string value;
					auxStack.pop(value);
					auxStack.push(to_string(sqrt(stod(value))));
				}
				else if (token == "!") {
					string value;
					auxStack.pop(value);
					auxStack.push(to_string(factorial(stoi(value))));
				}
				else {
					cout << "(Error) The user has not input sufficient values in the expression.";
					exit(EXIT_FAILURE);
				}
			}
			else if (auxStack.size() == 0) {
				cout << "(Error) The user has not input sufficient values in the expression.";
				exit(EXIT_FAILURE);
			}
		}
		else
			auxStack.push(token);
	}
	if (auxStack.size() == 1)
		return stod(auxStack.first());
	else {
		cout << "(Error) The user's input has too many values.";
		exit(EXIT_FAILURE);
	}
}

/*------------------------------------------------------------------
 factorial returns the factorial of a given integer number n.
 
 Preconditions: none.
 Postconditions: none.
 ------------------------------------------------------------------*/
int factorial(int n){
	if (n != 1)
		return(n * factorial(n - 1));
	return 1;
}