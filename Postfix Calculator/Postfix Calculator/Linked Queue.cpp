/*------------------------------------------------------------------
 Linked Queue.cpp
 Postfix Calculator
 
 A standard queue implementation using a linked-list implementation.
 
 Created by Neemias Freitas
 ------------------------------------------------------------------*/

#include "Linked Queue.h"

/*------------------------------------------------------------------
 The default LQueue constructor.
 
 Preconditions: none.
 Postconditions: queueSize = 0 and tail = NULL.
 ------------------------------------------------------------------*/
LQueue::LQueue(void) {
	tail = NULL;
	queueSize = 0;
}

/*------------------------------------------------------------------
 The default LQueue destructor.
 
 Preconditions: none.
 Postconditions: none
 ------------------------------------------------------------------*/
LQueue::~LQueue(void) {
	string token;
	while (dequeue(token));
}

/*------------------------------------------------------------------
 enqueue inserts a new string into the queue.
 
 Preconditions: none.
 Postconditions: queueSize ++, tail = temp.
 ------------------------------------------------------------------*/
void LQueue::enqueue(string token) {
	queueNode *temp = new queueNode;
	temp->token = token;
	
	if (tail == NULL)
		temp->next = temp;
	else {
		temp->next = tail->next;
		tail->next = temp;
	}
	tail = temp;
	queueSize++;
}

/*------------------------------------------------------------------
 dequeue returns to the user the string of head->value (if defined).
 
 Preconditions: none.
 Postconditions: queueSize--, tail->next=tail->next->next if tail != NULL,
 then return true; o.w. return false.
 ------------------------------------------------------------------*/
bool LQueue::dequeue(string &token) {
	if (tail != NULL) {
		token = tail->next->token;
		
		if (tail->next == tail)
			tail = NULL;
		else {
			queueNode *head = tail->next;
			tail->next = tail->next->next;
			delete head;
		}
		queueSize--;
		return true;
	}
	return false;
}

/*------------------------------------------------------------------
 first returns to the user the string of tail->next->token (if tail != NULL). Otherwise, returns the string "NULL".
 
 Preconditions: none.
 Postconditions: none.
 ------------------------------------------------------------------*/
string LQueue::first(void) const {
	if (tail != NULL)
		return tail->next->token;
	else
		return "NULL";
}

/*------------------------------------------------------------------
 size returns the number of strings currently on the queue.
 
 Preconditions: none.
 Postconditions: none.
 ------------------------------------------------------------------*/
int LQueue::size(void) const {
	return queueSize;
}

/*------------------------------------------------------------------
 print displays the content of the queue.
 
 Preconditions: none.
 Postconditions: none.
 ------------------------------------------------------------------*/
void LQueue::print (void) const {
	if (tail != NULL) {
		cout << "\nSize of the queue: " << queueSize << endl;
		int pos = 0;
		queueNode *current = tail->next;
		do {
			cout << "Position " << pos << " contains: " << current->token << endl;
			current = current->next;
			pos++;
		} while (current != tail->next);
	}
	else
		cout << "\nQueue is empty!\n";
}

/*------------------------------------------------------------------
 exportToString returns a single string which is composed by the
 concatenation of all strings stored in the queue.
 
 Preconditions: none.
 Postconditions: none.
 ------------------------------------------------------------------*/
string LQueue::exportToString(void) const {
	if (tail != NULL) {
		string str = "";
		queueNode *current = tail->next;
		do {
			str+= current->token + " ";
			current = current->next;
		} while (current != tail->next);
		return str;
	}
	else
		return "Queue is empty!";
}