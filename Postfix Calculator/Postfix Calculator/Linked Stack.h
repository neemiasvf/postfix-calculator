/*------------------------------------------------------------------
 Linked Stack.h
 Postfix Calculator
 
 A standard linked-list implementation of a stack class. Given the
 linked-list implementation, there is no upper limit on the number
 of elements that the stack can contain.
 
 Created by Neemias Freitas
 ------------------------------------------------------------------*/

#ifndef __Postfix_Calculator__Linked_Stack__
#define __Postfix_Calculator__Linked_Stack__

#include <iostream>
#include <string>
using namespace std;

// Definition of a stack node.
struct stackNode {
	string token;
	stackNode *next;
};

class LStack {
	
public:
	// Constructors & destructors
	/*--------------------------------------------------------------
	 The default stack constructor provides the caller with an
	 initially empty stack.
	 --------------------------------------------------------------*/
	LStack(void);
	
	/*--------------------------------------------------------------
	 The default stack destructor: deallocates all dynamically
	 allocated memory (i.e. stack nodes).
	 --------------------------------------------------------------*/
	~LStack(void);
	
	
	// Modification Methods
	/*--------------------------------------------------------------
	 The push method adds the provided string (token) to the stack.
	 
	 Precondition: none.
	 Postcondition: The number of elements being stored on the stack
	 is one greater than before.
	 --------------------------------------------------------------*/
	void push(string token);
	
	/*--------------------------------------------------------------
	 The pop method removes the most recently added string (i.e. the top string), and returns its content to
	 the caller, via the reference parameter.
	 If the stack is empty, the return value is false, o.w. true.
	 
	 Precondition: none.
	 Postcondition: The number of elements being stored in the stack
	 is one less than before.
	 --------------------------------------------------------------*/
	bool pop(string &token);
	
	
	// Constant Methods
	/*--------------------------------------------------------------
	 The first method does not alter the contents of the stack.  It
	 returns to the caller the string that will be returned by the next dequeue.
	 
	 Preconditions: none.
	 Postconditions: none.
	 --------------------------------------------------------------*/
	string first(void) const;
	
	/*--------------------------------------------------------------
	 size is a constant member function that returns
	 the number of strings currently stored in the stack.
	 
	 Preconditions: none.
	 Postconditions: none.
	 --------------------------------------------------------------*/
	int size(void) const;
	
	/*------------------------------------------------------------------
	 print displays the content of the stack.
	 
	 Preconditions: none.
	 Postconditions: none.
	 ------------------------------------------------------------------*/
	void print(void) const;
	
	
private:
	// Object instance data
	stackNode *top;
	int stackSize;
};

#endif /* defined(__Postfix_Calculator__Linked_Stack__) */